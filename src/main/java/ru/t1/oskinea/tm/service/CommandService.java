package ru.t1.oskinea.tm.service;

import ru.t1.oskinea.tm.api.repository.ICommandRepository;
import ru.t1.oskinea.tm.api.service.ICommandService;
import ru.t1.oskinea.tm.model.Command;

import static ru.t1.oskinea.tm.util.FormatUtil.formatBytes;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
